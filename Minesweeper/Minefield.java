
import java.util.ArrayList;

/**
 * Game Field.
 */
public class Minefield {

	private final int max_x; // Width
	private final int max_y; // Height

	MineCell[][] board;

	public Minefield(int width, int height) {
		this.max_x = width;
		this.max_y = height;
		this.board = new MineCell[max_x][max_y];
		for (int x = 0; x < max_x; x++) {
			for (int y = 0; y < max_y; y++) {
				board[x][y] = new MineCell(new Position(x, y));
			}
		}
	}

	public ArrayList<MineCell> getCells() {
		ArrayList<MineCell> result = new ArrayList<>(max_x * max_y);
		for (int x = 0; x < max_x; x++) {
			for (int y = 0; y < max_y; y++) {
				result.add(board[x][y]);
			}
		}
		return result;
	}

	public MineCell getCell(Position pos) {
		if (pos.getX() >= 0 && pos.getX() < max_x && pos.getY() >= 0 && pos.getY() < max_y) {
			return board[pos.getX()][pos.getY()];
		} else {
			return null;
		}
	}

	public ArrayList<MineCell> getHiddenCells() {
		ArrayList<MineCell> result = new ArrayList<>();
		for (int x = 0; x < max_x; x++) {
			for (int y = 0; y < max_y; y++) {
				if (board[x][y].isHidden())
					result.add(board[x][y]);
			}
		}
		return result;
	}

	/**
	 * Calculates all neighbors of a cell.
	 *
	 * @param pos whose neighbors are to be calculated on the field.
	 * @return all neighbors of {@code pos}, number of neighbors variable, pos not
	 *         included
	 */
	public ArrayList<MineCell> getNachbarn(Position pos) {
		ArrayList<MineCell> result = new ArrayList<>();
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if (x != 0 || y != 0) {
					MineCell nachbar = this.getCell(new Position(pos.getX() + x, pos.getY() + y));
					if (nachbar != null) {
						result.add(nachbar);
					}
				}
			}
		}
		return result;
	}

	public int getWidth() {
		return max_x;
	}

	public int getHeight() {
		return max_y;
	}
}
